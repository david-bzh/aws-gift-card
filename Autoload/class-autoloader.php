<?php
/**
 * Autoloader class
 *
 * @package Aws
 * @subpackage Aws\Autoload
 * @since 1.0.0
 */

namespace Aws\Autoload;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Aws autoloader.
 *
 * ElementorModules autoloader handler class is responsible for loading the different
 * classes needed to run the setup manager.
 *
 * @version 1.0.0
 */
class Autoloader {

	/**
	 * Run autoloader.
	 *
	 * Register a function as `__autoload()` implementation.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return void
	 */
	public static function run():void {
		spl_autoload_register( array( __CLASS__, 'autoload' ) );
	}

	/**
	 * Autoload.
	 *
	 * For a given class, check if it exist and load it.
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @param string $class Class name.
	 * @return void
	 */
	private static function autoload( $class ):void {

		$prefix_class = 'Aws';

		if ( 0 !== strpos( $class, $prefix_class ) ) {
			return;
		}

		$explode_class = explode( '\\', $class );

		unset( $explode_class[0] );

		$filename_class = 'class-' . strtolower( str_ireplace( '_', '-', $explode_class[ array_key_last( $explode_class ) ] ) ) . '.php';

		$path_file_class = dirname( __DIR__ ) . '/' . implode( '/', array_slice( $explode_class, 0, -1 ) ) . '/' . $filename_class;

		if ( is_readable( $path_file_class ) ) {
			require_once $path_file_class;
		}
	}
}
