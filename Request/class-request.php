<?php
/**
 * Aws request
 *
 * @package Aws
 * @subpackage Aws\Request
 * @since 1.0.0
 */

namespace Aws\Request;

/**
 * This class is in charge of send the request Api for AWS Incentives.
 *
 * @since 1.0.0
 */
class Request {

	/**
	 * Retrieve only the body from the raw response.
	 *
	 * @var null|object
	 */
	public $reponse = null;

	/**
	 * Initialize class Aws_Request.
	 *
	 * @param string $url Incentives API endpoint.
	 * @param array  $args Request parameters Api.
	 */
	public function __construct( $url, $args ) {

		$request = wp_remote_request(
			$url,
			$args
		);

		if ( ! is_wp_error( $request ) ) {
			$this->reponse = json_decode( wp_remote_retrieve_body( $request ) );
		}
	}
}
