<?php
/**
 * Gift Card
 *
 * @package Aws
 * @since 1.0.0
 */

namespace Aws;

use Roots\WPConfig\Config;
use Aws\Request\Request;
use Aws\Signature\Signature_V4;

/**
 * This class is in charge of send the request Api for AWS Incentives.
 *
 * @since 1.0.0
 */
class Gift_Card {

	/**
	 * Partner ID.
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/onboarding-process.html
	 *
	 * @var int
	 */
	private $partner_id = null;

	/**
	 * Initialize class Gift_Card.
	 */
	public function __construct() {

		// Initialize Autoload for the classes Aws.
		require 'Autoload/class-autoloader.php';
		Autoload\Autoloader::run();

		$this->partner_id = Config::get( 'AWS_PARTNER_ID' );
	}

	/**
	 * Create new grift card .
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/por-guide.html#creategiftcard
	 *
	 * @param string $creation_request_id A unique identifier for every CreateGiftCard request. You must generate a new value for every Create request (except for retries). (See notes below.).
	 * @param string $currency_code A ISO-4217 currency code that specifies the currency of the card.
	 * @param int    $amount Card amount, in card currency.
	 * @return null|object
	 */
	public function create_gift_card( $creation_request_id, $currency_code, $amount ) {
		return $this->request_gift_card(
			'/CreateGiftCard',
			array(
				'creationRequestId' => $this->partner_id . '-' . $creation_request_id,
				'partnerId'         => $this->partner_id,
				'value'             => array(
					'currencyCode' => $currency_code,
					'amount'       => floatval( $amount ),
				),
			)
		);
	}

	/**
	 * Activate Gift Card.
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/posa-gift-cards.html#activategiftcard
	 *
	 * @param string $activation_request_id A unique identifier for every ActivateGiftCard request. You must generate a new value for every Activate request (except for retries). (See notes below.).
	 * @param int    $card_number The 16-digit WAC number (example: 1400000005567585358).
	 * @param string $currency_code A ISO-4217 currency code that specifies the currency of the card.
	 * @param int    $amount Card amount, in card currency.
	 * @return null|object
	 */
	public function activate_gift_card( $activation_request_id, $card_number, $currency_code, $amount ) {
		return $this->request_gift_card(
			'/ActivateGiftCard',
			array(
				'activationRequestId' => $this->partner_id . '-' . $activation_request_id,
				'partnerId'           => $this->partner_id,
				'cardNumber'          => $card_number,
				'value'               =>
					array(
						'currencyCode' => $currency_code,
						'amount'       => floatval( $amount ),
					),
			)
		);
	}

	/**
	 * Activation Status Check.
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/posa-gift-cards.html#activationstatuscheck
	 *
	 * @param int $status_check_request_id $gc_request_id A unique identifier for every ActivationStatusCheck request. You must generate a new value for every Activated request (except for retries). (See notes below.).
	 * @param int $card_number The 16-digit WAC number (example: 1400000005567585358).
	 * @return null|object
	 */
	public function activation_status_check( $status_check_request_id, $card_number ) {
		return $this->request_gift_card(
			'/ActivationStatusCheck',
			array(
				'statusCheckRequestId' => $this->partner_id . '-' . $status_check_request_id,
				'partnerId'            => $this->partner_id,
				'cardNumber'           => $card_number,
			)
		);
	}

	/**
	 * Deactivate gift_card
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/posa-gift-cards.html#deactivategiftcard
	 *
	 * @param string $activation_request_id A unique identifier for every ActivateGiftCard request. You must generate a new value for every Activate request (except for retries). (See notes below.).
	 * @param int    $card_number The 16-digit WAC number (example: 1400000005567585358).
	 * @param string $currency_code A ISO-4217 currency code that specifies the currency of the card.
	 * @param int    $amount Card amount, in card currency.
	 * @return null|object
	 */
	public function deactivate_gift_card( $activation_request_id, $card_number, $currency_code, $amount ) {
		return $this->request_gift_card(
			'/DeactivateGiftCard',
			array(
				'activationRequestId' => $this->partner_id . '-' . $activation_request_id,
				'partnerId'           => $this->partner_id,
				'cardNumber'          => $card_number,
				'value'               =>
					array(
						'currencyCode' => $currency_code,
						'amount'       => floatval( $amount ),
					),
			)
		);
	}

	/**
	 * Cancel gift card
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/por-guide.html#cancelgiftcard
	 *
	 * @param string $creation_request_id A unique identifier for every CreateGiftCard request. You must generate a new value for every Create request (except for retries). (See notes below.).
	 * @return null|object
	 */
	public function cancel_gift_card( $creation_request_id ) {
		return $this->request_gift_card(
			'/CancelGiftCard',
			array(
				array(
					'creationRequestId' => $this->partner_id . '-' . $creation_request_id,
					'partnerId'         => $this->partner_id,
				),
			)
		);
	}

	/**
	 * Request gift card
	 *
	 * @param string $endpoint Incentives API endpoint.
	 * @param array  $payload Value from the payload in the body of the HTTP or HTTPS request.
	 * @return null|object
	 */
	private function request_gift_card( $endpoint, $payload ) {

		$signature_v4 = new Signature_V4( $endpoint, $payload );
		$request      = new Request( $signature_v4->request_url, $signature_v4->request_parameters );

		return $request->reponse;
	}
}
