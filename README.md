# Aws gift card for Bedrock WordPress

The AWS SDK for PHP makes it easy for Incentives API with Bedrock WordPress.

## Install

1. Add folder aws-gift-card/ in ./themes/my-theme/classes/

2. In .env
```
# Amazon gift card
AWS_PARTNER_ID='my-partner-id'
AWS_ACCESS_KEY='my-access-key'
AWS_SECRET_ACCESS_KEY='my-secret-key'
AWS_REGION_NAME='my-region-name'
AWS_SERVICE_NAME='my-service-name'
```
3. In application.php
```
/**
 * Settings AWS
 */
Config::define('AWS_PARTNER_ID', env('AWS_PARTNER_ID'));
Config::define('AWS_ACCESS_KEY', env('AWS_ACCESS_KEY'));
Config::define('AWS_SECRET_ACCESS_KEY', env('AWS_SECRET_ACCESS_KEY'));
Config::define('AWS_REGION_NAME', env('AWS_REGION_NAME'));
Config::define('AWS_SERVICE_NAME', env('AWS_SERVICE_NAME'));
```
4. In .\themes\my-theme\functions.php
```
require get_template_directory() . '/classes/aws-gift-card/class-gift-card.php';
```

## Quick Examples

### Create gift card

```php
$create_gift_card = ( new Aws\Gift_Card() )->create_gift_card( '1234', 'USD', 10 );
```

### Activate gift card

```php
$activate_gift_card = ( new Aws\Gift_Card() )->activate_gift_card( '1234', '1400000005567585358', 'USD', 10 );
```

### Activation status check gift card

```php
$activation_status_check = ( new Aws\Gift_Card() )->activation_status_check( '1234', '1400000005567585358', );
```

### Deactivate gift card

```php
$deactivate_gift_card = ( new Aws\Gift_Card() )->deactivate_gift_card( '1234', '1400000005567585358', 'USD', 10 );
```

### Cancel gift card

```php
$cancel_gift_card = ( new Aws\Gift_Card() )->cancel_gift_card( '1234' );
```
