<?php
/**
 * Aws Signature V4
 *
 * @link https://developer.amazon.com/docs/incentives-api/gift-codes-on-demand.html#known-answer-v4-signature-example
 *
 * @package Aws
 * @subpackage Aws\Signature
 * @since 1.0.0
 */

namespace Aws\Signature;

use Aws\Signature\Tasks\Calculating_Signature;
use Aws\Signature\Tasks\Canonical_Request;
use Aws\Signature\Tasks\String_To_Sign;
use Roots\WPConfig\Config;

/**
 * This class is in charge of gernerate Aws Signature V4
 *
 * @since 1.0.0
 */
class Signature_V4 {

	/**
	 * Request url for Incentives Api.
	 *
	 * @source https://developer.amazon.com/fr/docs/incentives-api/onboarding-process.html
	 *
	 * @var string
	 */
	public $request_url = 'https://agcod-v2-eu-gamma.amazon.com';

	/**
	 * Endpoint Incentives API endpoint.
	 *
	 * @var string
	 */
	private $endpoint = '';

	/**
	 * Request parameters Api.
	 *
	 * @var array
	 */
	public $request_parameters = array(
		'method'  => 'POST',
		'headers' => array(
			'accept'       => 'application/json',
			'content-type' => 'application/json',
			'host'         => 'agcod-v2-eu-gamma.amazon.com',
		),
	);

	/**
	 * Initialize class Aws_Request.
	 *
	 * @param string $endpoint Incentives API endpoint.
	 * @param array  $payload Value from the payload in the body of the HTTP or HTTPS request.
	 */
	public function __construct( $endpoint, $payload ) {

		$this->request_url               .= $endpoint;
		$this->endpoint                   = $endpoint;
		$this->request_parameters['body'] = json_encode( $payload ); // phpcs:ignore
		$this->set_headers();

		// Task 1 Create the canonical request.
		$canonical_request = new Canonical_Request( $endpoint, $this->request_parameters );

		// Task 2 Create the String-to-Sign.
		$string_to_sign = new String_To_Sign( $canonical_request->canonical_hashed, $this->request_parameters );

		// Task 3 Calculating the Signature.
		$calculating_signature = ( new Calculating_Signature( $string_to_sign ) );

		// Concatenate header Authorization.
		$this->request_parameters['headers']['authorization'] = $string_to_sign->algorithm . ' Credential=' . Config::get( 'AWS_ACCESS_KEY' ) . '/' . $string_to_sign->credential_scope . ', SignedHeaders=' . $canonical_request->signed_headers . ', Signature=' . $calculating_signature->signature;
	}

	/**
	 * Set headers Api
	 *
	 * @source https://developer.amazon.com/docs/incentives-api/incentives-api.html
	 *
	 * @return void
	 */
	private function set_headers() {

		$this->request_parameters['headers'] = array_merge(
			$this->request_parameters['headers'],
			array(
				'x-amz-date'   => gmdate( 'Ymd\THis\Z' ),
				'x-amz-target' => 'com.amazonaws.agcod.' . Config::get( 'AWS_SERVICE_NAME' ) . '.' . ltrim( $this->endpoint, '/' ),
			),
		);
	}
}
