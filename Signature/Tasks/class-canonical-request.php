<?php
/**
 * Canonical Request
 *
 * @link https://s3.amazonaws.com/AGCOD/tech_spec/CreatingV4SigForAGCOD.png
 *
 * @package Aws
 * @subpackage Aws\Signature\Tasks
 * @since 1.0.0
 */

namespace Aws\Signature\Tasks;

/**
 * This class is in charge of make the task 1 for Aws Signature V4
 *
 * @since 1.0.0
 */
class Canonical_Request {

	/**
	 * The canonical query string.
	 *
	 * @var string
	 */
	public $canonical_hashed = '';

	/**
	 * The canonical query string.
	 *
	 * @var string
	 */
	public $signed_headers = '';

	/**
	 * Initialize class Canonical_Request.
	 *
	 * @param string $endpoint Incentives API endpoint.
	 * @param array  $request_headers The Incentives API requires the following headers in each HTTP request.
	 */
	public function __construct( $endpoint, $request_headers ) {

		/* Step B1 Start with the HTTP request method (GET, PUT, POST, etc.), followed by a newline character. */
		$canonical_request[] = $request_headers['method'];

		/* Step B2 Add the canonical URI parameter, followed by a newline character. */
		$canonical_request[] = $endpoint;

		/* Step B3 Add the canonical query string, followed by a newline character. */
		$canonical_request[] = '';

		/* Step B4 Add the canonical headers, followed by a newline character. */
		foreach ( $request_headers['headers'] as $header_parm => $value ) {
			$canonical_request[] = $header_parm . ':' . $value;
		}

		/* Step B5 Add the signed headers, followed by a newline character. */
		$this->signed_headers = implode( ';', array_keys( $request_headers['headers'] ) );
		$canonical_request[]  = "\n" . $this->signed_headers;

		/* Step B.6 Use a hash (digest) function like SHA256 to create a hashed value from the payload in the body of the HTTP or HTTPS. */
		$canonical_request[] = hash( 'sha256', $request_headers['body'] );

		$this->canonical_hashed = implode( "\n", $canonical_request );
	}
}

