<?php
/**
 * String To Sign
 *
 * @link https://s3.amazonaws.com/AGCOD/tech_spec/CreatingV4SigForAGCOD.png
 *
 * @package Aws
 * @subpackage Aws\Signature\Tasks
 * @since 1.0.0
 */

namespace Aws\Signature\Tasks;

use Roots\WPConfig\Config;

/**
 * This class is in charge of make the task 2 for Aws Signature V4
 *
 * @since 1.0.0
 */
class String_To_Sign {

	/**
	 * Algorithm to the hashing.
	 *
	 * @var string
	 */
	public $algorithm = 'AWS4-HMAC-SHA256';

	/**
	 * Credential scope.
	 *
	 * @var string
	 */
	public $credential_scope = '';

	/**
	 * String signed.
	 *
	 * @var string
	 */
	public $signed = '';

	/**
	 * Initialize class String_To_Sign.
	 *
	 * @param string $canonical_request_hashed The hashed canonical request.
	 * @param array  $request_headers The Incentives API requires the following headers in each HTTP request.
	 */
	public function __construct( $canonical_request_hashed, $request_headers ) {

		/* Step B1 Start with the algorithm designation, followed by a newline character. */
		$string_to_sign[] = $this->algorithm;

		/* Step B2 Append the request date value, followed by a newline character. */
		$string_to_sign[] = $request_headers['headers']['x-amz-date'];

		/* Step B3 Append the credential scope value, followed by a newline character. */
		$this->credential_scope = gmdate( 'Ymd' ) . '/' . Config::get( 'AWS_REGION_NAME' ) . '/' . Config::get( 'AWS_SERVICE_NAME' ) . '/aws4_request';
		$string_to_sign[]       = $this->credential_scope;

		/* Step B4 Append the hash of the canonical request that you created in Task 1: Create a Canonical Request for Signature Version 4. */
		$string_to_sign[] = hash( 'sha256', $canonical_request_hashed );

		$this->signed = implode( "\n", $string_to_sign );
	}

}
