<?php
/**
 * Calculating Signature
 *
 * @link https://s3.amazonaws.com/AGCOD/tech_spec/CreatingV4SigForAGCOD.png
 *
 * @package Aws
 * @subpackage Aws\Signature\Tasks
 * @since 1.0.0
 */

namespace Aws\Signature\Tasks;

use Roots\WPConfig\Config;

/**
 * This class is in charge of make the task 3 for Aws Signature V4
 *
 * @since 1.0.0
 */
class Calculating_Signature {

	/**
	 * Signature.
	 *
	 * @var string
	 */
	public $signature = '';

	/**
	 * Initialize class Calculating_Signature.
	 *
	 * @param String_To_Sign $string_to_sign String to sign.
	 */
	public function __construct( $string_to_sign ) {

		/* Step C1.2 hash with the algorithm sha256 */
		$k_date = hash_hmac( 'sha256', gmdate( 'Ymd' ), 'AWS4' . Config::get( 'AWS_SECRET_ACCESS_KEY' ), true );
		/* Step C1.3 hash with the algorithm sha256 */
		$k_region = hash_hmac( 'sha256', Config::get( 'AWS_REGION_NAME' ), $k_date, true );
		/* Step C1.4 hash with the algorithm sha256 */
		$k_service = hash_hmac( 'sha256', Config::get( 'AWS_SERVICE_NAME' ), $k_region, true );
		/* Step C1.5 hash with the algorithm sha256 */
		$k_signing = hash_hmac( 'sha256', 'aws4_request', $k_service, true );
		/* Step C2 Calculate the signature. */
		$this->signature = hash_hmac( 'sha256', $string_to_sign->signed, $k_signing );
	}
}
